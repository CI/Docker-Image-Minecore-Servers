#!/bin/bash

# Check if the environment variable is set
if [ -z "$HOSTNAME" ]; then
  echo "hostname environment variable is not set."
  exit 1
fi
# Get the hostname of the container
HOSTNAME=$(hostname)
# Path to the file you want to modify
mkdir -p "/data/plugins/ChatControlRed"
FILE_PATH="/data/plugins/ChatControlRed/bungee.yml"

# Replace the line in the file
sed -i "s/^Server_Name: server_name/Server_Name: $HOSTNAME/"  $FILE_PATH

FILE_PATH="/data/server.properties"

# Create or overwrite the server.properties file with the hostname
echo "server-name=$HOSTNAME" >> "$FILE_PATH"
echo "enforce-secure-profile=false" >> "$FILE_PATH"
echo "view-distance=6" >> "$FILE_PATH"
echo "difficulty=hard" >> "$FILE_PATH"
sed -i 's/^view-distance=10/view-distance=6/; s/^difficulty=easy/difficulty=hard/' "$FILE_PATH"
