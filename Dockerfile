FROM itzg/minecraft-server:java21-jdk

COPY adjust.sh /opt/adjust.sh
COPY start.sh /opt/start.sh
RUN chmod +x /opt/adjust.sh
RUN chmod +x /opt/start.sh

ENTRYPOINT ["/bin/bash", "/opt/start.sh"]
